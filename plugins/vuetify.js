import Vue from 'vue'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'

// import CSS
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify, {
  theme: {
    primary: colors.indigo.base,        // #3F51B5
    accent: colors.deepPurple.lighten1, // #7E57C2
    secondary: colors.blue.base,        // #2196F3
    info: colors.teal.lighten1,         // #26A69A
    warning: colors.amber.base,         // #FFC107
    error: '#921A55',                   // Maroon
    success: colors.green.accent3       // #00E676
  }
})
